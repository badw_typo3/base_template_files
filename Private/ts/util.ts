################################################################################
# Menue for Bootstrap Layout 
# original version retrieved from 
# https://typo3.org/documentation/snippets/sd/444 (25.03.2015)
################################################################################

########################################
# header navigation
########################################
lib.menu = HMENU
lib.menu {
  1 = TMENU
  1 {
    NO.allWrap = <li>|</li>
    NO.ATagTitle.field = abstract // description // title
  } 
  wrap = <ul class="nav navbar-nav navbar-right">|</ul>
}


########################################
# left side navigation, can be called with
# <f:cObject typoscriptObjectPath="lib.nav"/> 
# from inside a fluid-html-template
########################################

lib.nav = HMENU 
lib.nav { 
  1 = TMENU
  1 {
    expAll = 1
 
    NO.allWrap = <li>|</li>
    NO.ATagTitle.field = abstract // description // title

    ACT = 1 
    ACT {
      allWrap = <li class="active">|</li> 
      ATagTitle.field = abstract // description // title 
    } 
 
    IFSUB = 1
    IFSUB {
      before = <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      after = <b class="caret"></b></a>
      doNotLinkIt = 1
      wrapItemAndSub = <li class="dropdown">|</li>
      ATagTitle.field = abstract // description // title
    }

    ACTIFSUB = 1
    ACTIFSUB {
      before = <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      after = <b class="caret"></b></a>
      doNotLinkIt = 1
      wrapItemAndSub = <li class="dropdown active">|</li>
      ATagTitle.field = abstract // description // title
    }

    wrap = <ul class="nav nav-pills nav-stacked">|</ul>
  } 
 
  2 = TMENU
  2 {
    expAll = 1
     
    ACT = 1
    ACT {
      wrapItemAndSub = <li class="active">|</li>
      ATagTitle.field = abstract // description // title
    }

    ACTIFSUB = 1
    ACTIFSUB {
      wrapItemAndSub = |
      before = <li class="divider"></li><li class="nav-header">
      after = </li>
      doNotLinkIt = 1
      ATagTitle.field = abstract // description // title
    }

    NO  {
      allWrap = <li>|</li>
      ATagTitle.field = abstract // description // title
    }
     
    IFSUB = 1
    IFSUB {
      before = <li class="divider"></li><li class="nav-header">
      after = </li>
      doNotLinkIt = 1
      ATagTitle.field = abstract // description // title
    }

    SPC = 1
    SPC {
      allWrap = <li class="divider"></li><li class="nav-header">|</li>
    }
 
    wrap = <ul class="dropdown-menu">|</ul>
  }
 
  3 = TMENU
  3 {
    NO {
      allWrap = <li>|</li>
      ATagTitle.field = abstract // description // title
    }

    ACT = 1
    ACT {
      wrapItemAndSub = <li class="active">|</li>
      ATagTitle.field = abstract // description // title
    }
  } 
}
########################################
# SEARCHBOX
# retrieved from http://www.typo3wizard.com/de/snippets/cool-stuff-typoscript/ts-basierte-searchbox-fuer-indexed-search.html
########################################

lib.searchbox = COA_INT
lib.searchbox {
  stdWrap.prefixComment = 2 | lib.searchbox
  10 = TEXT
  10.typolink.parameter = {$plugin.tx_indexedsearch.searchUID}
  10.typolink.returnLast = url
  10.wrap = <div id="indexedsearchbox"><form action="|" method="post" id="indexedsearch"><table cellpadding="0" cellspacing="0" border="0">
  20 = COA
  20 {
    wrap = <tr> | </tr>
    10 = TEXT
    10.data = GPvar : tx_indexedsearch |sword
    10.htmlSpecialChars = 1
    10.wrap = <td><input name="tx_indexedsearch[sword]" value="|" class="searchbox-sword" type="text" /></td>
    20 = COA
    20 {
      wrap = <td align="right">&nbsp;|</td>
      10 = TEXT
      10.value = <input type="hidden" name="tx_indexedsearch[sections]" value="0" />
      20 = TEXT
      20.value = <input name="tx_indexedsearch[submit_button]" value="Search" type="hidden" />
      30 = TEXT
      #entweder Bild in Fileadmin ablegen oder rausnehmen
      #30.value = <input name="search" src="fileadmin/templates/images/search.gif" value="Search" class="searchbox-button" type="image" />
      #30.value = <input name="search" value="Search" class="searchbox-button" type="image" />
    }
  }
  30 = COA
  30 {
    wrap = <tr>|</tr>
    10 = TEXT
    10.value = Advanced search »
    10.typolink.parameter = {$plugin.tx_indexedsearch.searchUID}
    10.typolink.additionalParams = &tx_indexedsearch[ext]=1
    10.wrap = <td align="right" colspan="2">|</td>
    if.isTrue = {$plugin.tx_indexedsearch.showAdvanced}
  }
  wrap = | </table></form></div>
}
