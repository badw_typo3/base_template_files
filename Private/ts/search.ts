## Search Box ###################################################################
lib.searchbox = COA_INT
lib.searchbox {
    stdWrap.prefixComment = 2 | lib.searchbox
    10 = TEXT
    10.typolink.parameter = {$plugin.tx_indexedsearch.searchUID}
    10.typolink.returnLast = url
    10.wrap = <div id="indexedsearchbox"><form action="|" method="post" id="indexedsearch"><table cellpadding="0" cellspacing="0" border="0">
    20 = COA
    20 {
        wrap = <tr> | </tr>
        10 = TEXT
        10.data = GPvar : tx_indexedsearch |sword
        10.htmlSpecialChars = 1
        10.wrap = <td><input name="tx_indexedsearch[sword]" value="Suche ..." class="searchbox-sword" type="text" /></td>
        20 = COA
        20 {
            wrap = <td align="right">&nbsp;|</td>
           10 = TEXT
           10.value = <input type="hidden" name="tx_indexedsearch[sections]" value="0" />
           20 = TEXT
           20.value = <input name="tx_indexedsearch[submit_button]" value="Search" type="hidden" />
#          30 = TEXT
#          30.value = <input name="search" src="fileadmin/template-responsive/css/images/search_red.png" value="Search" class="searchbox-button" type="image" />
        }
      }
    30 = COA
    30 {
        wrap = <tr>|</tr>
        10 = TEXT
        10.value = Advanced search »
        10.typolink.parameter = {$plugin.tx_indexedsearch.searchUID}
        10.typolink.additionalParams = &tx_indexedsearch[ext]=1
        10.wrap = <td align="right" colspan="2">|</td>
        if.isTrue = {$plugin.tx_indexedsearch.showAdvanced}
     }
wrap = | </table></form></div>
}