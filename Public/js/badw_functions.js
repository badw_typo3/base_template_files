/**
 * Diese Datei liefert die zusaetzlichen javascript Funktionen, die die extension benoetigt.
 * z.B. fuer die EventHandler 
 */
$(document).ready(function () {
    //setEventHandler();
    //die EventHandler
    setTheFirstFocus();
    setChangeCommitEvent();
    scrollToTheRecords();
    setGoUpEvent();
    setResetFilterEvent();
});

function setEventHandler() {
    
    /**
     * dieser Part baut aus dem uebergebenen JSON-Array ein UI-Autocomplete widget 
     * fuer die Publikationen, Autoren
     */

    if (typeof autorList !== 'undefined' && typeof titelList !== 'undefined') {
        //die Autoren als Objekt
        var availableAuthors = $.parseJSON(autorList);
        //jetzt als array zur uebergabe an den ui handler
        availableAuthors = $.makeArray(availableAuthors);

        var availableTitel = $.parseJSON(titelList);
        var availableTitel = $.makeArray(availableTitel);

        $("#autor").autocomplete({
            source: availableAuthors
        });
        $("#titel").autocomplete({
            source: availableTitel
        });
    }
}

function setTheFirstFocus(){
    //beim Laden der Seite Publikationen Cursor in Autorenfeld
    if ($('#autor') !== null && $('#autor') !== 'undefined') {
        $('#autor').focus();
    }
    else if($('#titel') !== null && $('#titel') !== 'undefined'){
        $('#titel').focus();
    }
}

function scrollToTheRecords() {
    if (typeof $('.badw_recordsFound').get(0) !== 'undefined') {
        $('html,body').animate({scrollTop: ($('.badw_recordsFound').offset().top) - 50}, 'fast');
    }
}

function setChangeCommitEvent() {
    //submitten beim onchange event -> fuer input felder wenn geschrieben und focus out
    $("select.form-control, input.form-control").on('change', function () {
        //console.log($(this).parents() );
        $(this).parents('form').submit();
    });
}

function setResetFilterEvent() {
    //wenn filter Form vorhanden
    if (typeof document.getElementById('badw_filter_form') !== 'undefined'
            && typeof document.getElementById('resetFilterButton') !== 'undefined') {
        $('#resetFilterButton').click(function () {
            var $selectObj = $('#badw_filter_form').find('select');
            $selectObj.each(function () {
                //setze select auf den index 0 
                $(this).get(0).selectedIndex = 0;
            });
            var $inputFields = $('#badw_filter_form').find('input.badw_search_input');
            $inputFields.each(function () {
                $(this).val("");
            });
            //submit-feuern
            $('#badw_filter_form').submit();
        });
    }
}

function setGoUpEvent(){
    if(typeof document.getElementById('goUp') !== 'undefined'){
        $('#goUp').click(function() {
            $('html,body').animate({scrollTop: 0}, 'slow');
        });
    }
}



